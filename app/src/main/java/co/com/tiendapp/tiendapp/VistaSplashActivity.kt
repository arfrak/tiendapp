package co.com.tiendapp.tiendapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import java.lang.Exception

class VistaSplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vista_splash)

        getTransition()
    }

    private fun getTransition() {

        val background = object : Thread() {

            override fun run() {

                try {

                    Thread.sleep(3000)

                    val intentVistaPrincipal = Intent(baseContext, VistaHomeActivity::class.java)
                    startActivity(intentVistaPrincipal)

                } catch (e: Exception) {

                    e.printStackTrace()
                }
            }
        }
        background.start()
    }
}
