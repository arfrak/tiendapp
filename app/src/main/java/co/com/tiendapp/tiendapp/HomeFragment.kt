package co.com.tiendapp.tiendapp

import Adapters.ViewPageAdapter
import Objects.Response
import Util.Util
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson

class HomeFragment : Fragment() {

    lateinit var vistaHome : View
    lateinit var viewPager : ViewPager
    lateinit var mainGrid: GridLayout
    lateinit var typefaceView: Typeface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // SE INSTANCIA LA VISTA PARA CONFIGURACION GENERAL HACIENDO USO DE ELLA
        vistaHome = inflater.inflate(R.layout.fragment_home, container, false)

        setViewComponents()

        // Inflate the layout for this fragment
        return vistaHome
    }

    private fun setViewComponents() {

        typefaceView = Typeface.createFromAsset(vistaHome.context.assets, "fonts/SF-Pro-Display-Regular.otf")

        // SE CARGAN LAS IMAGENES AL ADAPTER (BANNER)
        viewPager = vistaHome.findViewById(R.id.vistaBannerImagenes) as ViewPager
        val adapter = ViewPageAdapter(vistaHome.context)
        viewPager.adapter = adapter

        loadProductsFilter()
    }

    private fun loadProductsFilter() {

        var position = 0
        mainGrid = vistaHome.findViewById(R.id.mainGrid) as GridLayout

        // SE CARGAN LOS PRODUCTOS DEL ARCHIVO JSON
        val gson = Gson()
        val json = Util().loadData("productos.json", vistaHome.context)

        val products = gson.fromJson(json, Response::class.java)

        if(products.productos?.size!! > 0) {

            mainGrid.visibility = View.VISIBLE

            // SE OBTIENE LA LISTA DE PRODUCTOS Y SE CARGA EN LA VISTA
            for (producto in products.productos!!) {

                if (position == 0) {

                    val tvCategoriaProducto   = vistaHome.findViewById<TextView>(R.id.tvCategoriaProducto1)
                    tvCategoriaProducto.setTypeface(typefaceView)
                    tvCategoriaProducto.setText(producto.categoria)

                    val tvDescripcionProducto = vistaHome.findViewById<TextView>(R.id.tvDescripcionProducto1)
                    tvDescripcionProducto.setTypeface(typefaceView)
                    tvDescripcionProducto.setText(producto.descripcion)

                    val tvMarcaProducto       = vistaHome.findViewById<TextView>(R.id.tvMarcaProducto1)
                    tvMarcaProducto.setTypeface(typefaceView)
                    tvMarcaProducto.setText(producto.marca)

                    val ivImagenProducto      = vistaHome.findViewById<ImageView>(R.id.ivImagenProducto1)
                    ivImagenProducto.setImageResource(R.drawable.imagen1)

                    val tvPrecioProducto      = vistaHome.findViewById<TextView>(R.id.tvPrecioProducto1)
                    tvPrecioProducto.setTypeface(typefaceView)
                    tvPrecioProducto.setText(producto.precio)

                    val cvProducto = vistaHome.findViewById<CardView>(R.id.cvProducto1)
                    cvProducto.setOnClickListener {

                        val jsonProduct: String = gson.toJson(producto)

                        val intent = Intent(vistaHome.context, ProductDetailActivity::class.java)
                        intent.putExtra("PRODUCTO", jsonProduct)
                        startActivity(intent)
                    }
                }

                if (position == 1) {

                    val tvCategoriaProducto   = vistaHome.findViewById<TextView>(R.id.tvCategoriaProducto2)
                    tvCategoriaProducto.setTypeface(typefaceView)
                    tvCategoriaProducto.setText(producto.categoria.toString())

                    val tvDescripcionProducto = vistaHome.findViewById<TextView>(R.id.tvDescripcionProducto2)
                    tvDescripcionProducto.setTypeface(typefaceView)
                    tvDescripcionProducto.setText(producto.descripcion)

                    val tvMarcaProducto       = vistaHome.findViewById<TextView>(R.id.tvMarcaProducto2)
                    tvMarcaProducto.setTypeface(typefaceView)
                    tvMarcaProducto.setText(producto.marca)

                    val ivImagenProducto      = vistaHome.findViewById<ImageView>(R.id.ivImagenProducto2)
                    ivImagenProducto.setImageResource(R.drawable.imagen2)

                    val tvPrecioProducto     = vistaHome.findViewById<TextView>(R.id.tvPrecioProducto2)
                    tvPrecioProducto.setTypeface(typefaceView)
                    tvPrecioProducto.setText(producto.precio)

                    val cvProducto = vistaHome.findViewById<CardView>(R.id.cvProducto2)
                    cvProducto.setOnClickListener {

                        val jsonProduct: String = gson.toJson(producto)

                        val intent = Intent(vistaHome.context, ProductDetailActivity::class.java)
                        intent.putExtra("PRODUCTO", jsonProduct)
                        startActivity(intent)
                    }
                }

                if (position == 2) {

                    val tvCategoriaProducto   = vistaHome.findViewById<TextView>(R.id.tvCategoriaProducto3)
                    tvCategoriaProducto.setTypeface(typefaceView)
                    tvCategoriaProducto.setText(producto.categoria.toString())

                    val tvDescripcionProducto = vistaHome.findViewById<TextView>(R.id.tvDescripcionProducto3)
                    tvDescripcionProducto.setTypeface(typefaceView)
                    tvDescripcionProducto.setText(producto.descripcion)

                    val tvMarcaProducto       = vistaHome.findViewById<TextView>(R.id.tvMarcaProducto3)
                    tvMarcaProducto.setTypeface(typefaceView)
                    tvMarcaProducto.setText(producto.marca)

                    val ivImagenProducto      = vistaHome.findViewById<ImageView>(R.id.ivImagenProducto3)
                    ivImagenProducto.setImageResource(R.drawable.imagen3)

                    val tvPrecioProducto      = vistaHome.findViewById<TextView>(R.id.tvPrecioProducto3)
                    tvPrecioProducto.setTypeface(typefaceView)
                    tvPrecioProducto.setText(producto.precio)

                    val cvProducto = vistaHome.findViewById<CardView>(R.id.cvProducto3)
                    cvProducto.setOnClickListener {

                        val jsonProduct: String = gson.toJson(producto)

                        val intent = Intent(vistaHome.context, ProductDetailActivity::class.java)
                        intent.putExtra("PRODUCTO", jsonProduct)
                        startActivity(intent)
                    }
                }

                if (position == 3) {

                    val tvCategoriaProducto   = vistaHome.findViewById<TextView>(R.id.tvCategoriaProducto4)
                    tvCategoriaProducto.setTypeface(typefaceView)
                    tvCategoriaProducto.setText(producto.categoria.toString())

                    val tvDescripcionProducto = vistaHome.findViewById<TextView>(R.id.tvDescripcionProducto4)
                    tvDescripcionProducto.setTypeface(typefaceView)
                    tvDescripcionProducto.setText(producto.descripcion)

                    val tvMarcaProducto       = vistaHome.findViewById<TextView>(R.id.tvMarcaProducto4)
                    tvMarcaProducto.setTypeface(typefaceView)
                    tvMarcaProducto.setText(producto.marca)

                    val ivImagenProducto      = vistaHome.findViewById<ImageView>(R.id.ivImagenProducto4)
                    ivImagenProducto.setImageResource(R.drawable.imagen4)

                    val tvPrecioProducto      = vistaHome.findViewById<TextView>(R.id.tvPrecioProducto4)
                    tvPrecioProducto.setTypeface(typefaceView)
                    tvPrecioProducto.setText(producto.precio)

                    val cvProducto = vistaHome.findViewById<CardView>(R.id.cvProducto4)
                    cvProducto.setOnClickListener {

                        val jsonProduct: String = gson.toJson(producto)

                        val intent = Intent(vistaHome.context, ProductDetailActivity::class.java)
                        intent.putExtra("PRODUCTO", jsonProduct)
                        startActivity(intent)
                    }
                }

                if (position == 4) {

                    val tvCategoriaProducto   = vistaHome.findViewById<TextView>(R.id.tvCategoriaProducto5)
                    tvCategoriaProducto.setTypeface(typefaceView)
                    tvCategoriaProducto.setText(producto.categoria.toString())

                    val tvDescripcionProducto = vistaHome.findViewById<TextView>(R.id.tvDescripcionProducto5)
                    tvDescripcionProducto.setTypeface(typefaceView)
                    tvDescripcionProducto.setText(producto.descripcion)

                    val tvMarcaProducto       = vistaHome.findViewById<TextView>(R.id.tvMarcaProducto5)
                    tvMarcaProducto.setTypeface(typefaceView)
                    tvMarcaProducto.setText(producto.marca)

                    val ivImagenProducto      = vistaHome.findViewById<ImageView>(R.id.ivImagenProducto5)
                    ivImagenProducto.setImageResource(R.drawable.imagen5)

                    val tvPrecioProducto      = vistaHome.findViewById<TextView>(R.id.tvPrecioProducto5)
                    tvPrecioProducto.setTypeface(typefaceView)
                    tvPrecioProducto.setText(producto.precio)

                    val cvProducto = vistaHome.findViewById<CardView>(R.id.cvProducto5)
                    cvProducto.setOnClickListener {

                        val jsonProduct: String = gson.toJson(producto)

                        val intent = Intent(vistaHome.context, ProductDetailActivity::class.java)
                        intent.putExtra("PRODUCTO", jsonProduct)
                        startActivity(intent)
                    }
                }

                if (position == 5) {

                    val tvCategoriaProducto   = vistaHome.findViewById<TextView>(R.id.tvCategoriaProducto6)
                    tvCategoriaProducto.setTypeface(typefaceView)
                    tvCategoriaProducto.setText(producto.categoria.toString())

                    val tvDescripcionProducto = vistaHome.findViewById<TextView>(R.id.tvDescripcionProducto6)
                    tvDescripcionProducto.setTypeface(typefaceView)
                    tvDescripcionProducto.setText(producto.descripcion)

                    val tvMarcaProducto       = vistaHome.findViewById<TextView>(R.id.tvMarcaProducto6)
                    tvMarcaProducto.setTypeface(typefaceView)
                    tvMarcaProducto.setText(producto.marca)

                    val ivImagenProducto      = vistaHome.findViewById<ImageView>(R.id.ivImagenProducto6)
                    ivImagenProducto.setImageResource(R.drawable.imagen6)

                    val tvPrecioProducto      = vistaHome.findViewById<TextView>(R.id.tvPrecioProducto6)
                    tvPrecioProducto.setTypeface(typefaceView)
                    tvPrecioProducto.setText(producto.precio)

                    val cvProducto = vistaHome.findViewById<CardView>(R.id.cvProducto6)
                    cvProducto.setOnClickListener {

                        val jsonProduct: String = gson.toJson(producto)

                        val intent = Intent(vistaHome.context, ProductDetailActivity::class.java)
                        intent.putExtra("PRODUCTO", jsonProduct)
                        startActivity(intent)
                    }
                }

                position += 1
            }

        } else {

            // SE MUESTRA UN MENSAJE DE NO EXISTENCIA DE PRODUCTOS
            mainGrid.visibility = View.GONE
        }
    }
}
