package co.com.tiendapp.tiendapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ComprasFragment : Fragment() {

    lateinit var vistaCompras : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        vistaCompras = inflater.inflate(R.layout.fragment_compras, container, false)

        // Inflate the layout for this fragment
        return vistaCompras
    }
}
