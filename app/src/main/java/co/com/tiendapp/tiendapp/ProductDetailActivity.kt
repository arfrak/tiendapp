package co.com.tiendapp.tiendapp

import Objects.Producto
import Objects.Response
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson

class ProductDetailActivity : AppCompatActivity() {

    lateinit var typefaceView: Typeface
    lateinit var producto: Producto

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        setViewComponents()
    }

    private fun setViewComponents() {

        // SE OBTIENE EL OBJETO DE LA VISTA DE LISTA DE PRODUCTOS
        val bundle = intent.extras
        val productoSrt: String

        if(bundle != null) {

            productoSrt = bundle.getString("PRODUCTO")

            val gson = Gson()
            producto = gson.fromJson(productoSrt, Producto::class.java)
        }

        typefaceView = Typeface.createFromAsset(baseContext.assets, "fonts/SF-Pro-Display-Regular.otf")

        val tvCategoriaProducto   = findViewById<TextView>(R.id.tvCategoriaProducto)
        tvCategoriaProducto.setTypeface(typefaceView)
        tvCategoriaProducto.setText(producto.categoria)

        val llCategoria = findViewById<LinearLayout>(R.id.llCategoria)
        val llBotonComprar = findViewById<LinearLayout>(R.id.llBotonComprar)

        if(producto.categoria.equals("Tecnologia")) {

            llCategoria.setBackgroundResource(R.drawable.btncategoria4)
            llBotonComprar.setBackgroundResource(R.drawable.btncategoria4)

        } else if(producto.categoria.equals("Hogar")) {

            llCategoria.setBackgroundResource(R.drawable.btncategoria3)
            llBotonComprar.setBackgroundResource(R.drawable.btncategoria3)

        } else {

            llCategoria.setBackgroundResource(R.drawable.btncategoria2)
            llBotonComprar.setBackgroundResource(R.drawable.btncategoria2)
        }

        val tvDescripcionProducto = findViewById<TextView>(R.id.tvDescripcionProducto)
        tvDescripcionProducto.setTypeface(typefaceView)
        tvDescripcionProducto.setText(producto.descripcion)

        val tvMarcaProducto       = findViewById<TextView>(R.id.tvMarcaProducto)
        tvMarcaProducto.setTypeface(typefaceView)
        tvMarcaProducto.setText(producto.marca)

        val ivImagenProducto      = findViewById<ImageView>(R.id.ivImagenProducto)

        if(producto.imagen.equals("imagen1")) {

            ivImagenProducto.setImageResource(R.drawable.imagen1)

        } else if(producto.imagen.equals("imagen2")) {

            ivImagenProducto.setImageResource(R.drawable.imagen2)

        } else if(producto.imagen.equals("imagen3")) {

            ivImagenProducto.setImageResource(R.drawable.imagen3)

        } else if(producto.imagen.equals("imagen4")) {

            ivImagenProducto.setImageResource(R.drawable.imagen4)

        } else if(producto.imagen.equals("imagen5")) {

            ivImagenProducto.setImageResource(R.drawable.imagen5)

        } else {

            ivImagenProducto.setImageResource(R.drawable.imagen6)
        }

        val tvPrecioProducto      = findViewById<TextView>(R.id.tvPrecioProducto)
        tvPrecioProducto.setTypeface(typefaceView)
        tvPrecioProducto.setText(producto.precio)
    }

    fun onClickComprarProducto(view: View) {

        
    }
}
