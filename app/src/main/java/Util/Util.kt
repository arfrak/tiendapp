package Util

import android.content.Context
import java.io.IOException

class Util {

    fun loadData(inFile: String, context: Context): String {

        var tContents = ""

        try {

            val stream = context.assets.open(inFile)

            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()
            tContents = String(buffer)

        } catch (e: IOException) {

            tContents = "No hay productos."
        }

        return tContents
    }
}